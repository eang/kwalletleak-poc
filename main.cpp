#include <KWallet>

#include <QDebug>

using namespace KWallet;

int main(int argc, char **argv)
{
    auto wallet = Wallet::openWallet("kdewallet", 0, Wallet::Synchronous);
    if (!wallet) {
        qDebug() << "Failed to open KWallet";
        return -1;
    }

    foreach (const QString& folder, wallet->folderList()) {
        wallet->setFolder(folder);
        qDebug() << "Checking data in folder" << folder;

        foreach (const QString& entry, wallet->entryList()) {
            qDebug() << "Checking entry" << entry;

            QByteArray buffer;
            QMap<QString, QString> map;
            QString password;

            wallet->readEntry(entry, buffer);
            wallet->readMap(entry, map);
            wallet->readPassword(entry, password);

            qDebug() << "Raw data:" << QString::fromLatin1(buffer);
            qDebug() << "Mapped data:" << map;
            qDebug() << "Password:" << password;
        }
    }

    return 0;
}
